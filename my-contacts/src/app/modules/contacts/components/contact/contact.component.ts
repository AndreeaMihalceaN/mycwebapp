import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from 'src/app/core/services/validation.service';
import { Contact } from 'src/app/shared/models/contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @Input() set contact(contact: Contact) {
    if (contact) {
      this.contactForm.setValue({
        firstName: contact.firstName ? contact.firstName : '',
        lastName: contact.lastName ? contact.lastName : '',
        phoneNumber: contact.phoneNumber ? contact.phoneNumber : '',
        email: contact.email ? contact.email : '',
      });
      this.theContact.id = contact.id;
    }
  }
  @Input() buttonText = 'Submit';
  @Output() save: EventEmitter<Contact> = new EventEmitter<Contact>();

  private theContact = new Contact();

  public btnCLicked = false;
  public contactForm = new FormGroup({
    firstName: new FormControl(
      this.theContact.firstName,
      [Validators.required]
    ),
    lastName: new FormControl(
      this.theContact.lastName,
      [Validators.required]
    ),
    phoneNumber: new FormControl(
      this.theContact.phoneNumber,
      [Validators.required, Validators.maxLength(10)]
    ),
    email: new FormControl(
      this.theContact.email,
      [Validators.required, ValidationService.emailValidator]
    )
  });

  constructor() { }

  ngOnInit(): void {
    this.onChanges();
  }

  public get isFormValid(): boolean {
    return this.contactForm.valid;
  }

  public onSave(): void {
    this.save.emit(this.theContact);
  }

  private onChanges(): void {
    this.contactForm.valueChanges.subscribe(formValue => {
      this.theContact.firstName = formValue.firstName;
      this.theContact.lastName = formValue.lastName;
      this.theContact.phoneNumber = formValue.phoneNumber;
      this.theContact.email = formValue.email;
    });
  }
}
