import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from 'src/app/shared/models/contact';

@Component({
  selector: 'app-contacts-table',
  templateUrl: './contacts-table.component.html',
  styleUrls: ['./contacts-table.component.scss']
})
export class ContactsTableComponent implements OnInit {

  @Input() contacts: Contact[] = [];
  @Input() cols: string[];

  @Output() updateContact: EventEmitter<Contact> = new EventEmitter<Contact>();
  @Output() deleteContact: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  public oneditContact(contact: Contact): void {
    this.updateContact.emit(contact);
  }

  public ondeleteContact(index: number, contact: Contact): void {
    this.deleteContact.emit({ index, contact });
  }

}
