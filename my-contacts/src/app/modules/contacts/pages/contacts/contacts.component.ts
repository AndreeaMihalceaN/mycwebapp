import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { Router } from '@angular/router';
import { ContactService } from 'src/app/core/http/contact/contact.service';
import { Contact } from 'src/app/shared/models/contact';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  public contacts: Contact[] = [];
  public currentContact = new Contact();
  public isFormVisible = false;
  public cols = ['First Name', 'Last Name', 'Phone Number', 'Email'];
  public buttonText: 'Add Contact' | 'Cancel';
  public formButtonText: 'Submit' | 'Update';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private contactService: ContactService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAllContacts();
    this.buttonText = 'Add Contact';
    this.formButtonText = 'Submit';
  }

  public logout(): void {
    this.authenticationService.logout();
    this.router.navigateByUrl('/login');
  }

  public addContact(): void {
    this.isFormVisible = !this.isFormVisible;
    if (this.isFormVisible) {
      this.buttonText = 'Cancel';
      this.formButtonText = 'Submit';
    } else {
      this.buttonText = 'Add Contact';
      this.currentContact = new Contact();
    }
  }

  public deleteContact(event: { index: number, contact: Contact }): void {
    this.contactService.deleteContact(event.contact.id).subscribe(result => this.toastr.success('Contact Deleted!'));
    this.contacts.splice(event.index, 1);
  }

  public onUpdateContact(item: Contact): void {
    this.currentContact = item;
    this.isFormVisible = true;
    this.buttonText = 'Cancel';
    this.formButtonText = 'Update';
  }

  public onSaveContact(item: Contact): void {
    if (item.id) {
      this.updateContact(item);
    } else {
      this.saveContact(item);
      this.isFormVisible = false;
      this.buttonText = 'Add Contact';
    }
  }

  private updateContact(item: Contact): void {
    this.contactService.updateContact(item).subscribe(result => {
      this.toastr.success('Contact Updated!');
      this.getAllContacts();
      this.currentContact = new Contact();
    });
  }

  private saveContact(item: Contact): void {
    this.contactService.createContact(item).subscribe(result => {
      this.contacts.push(result);
      this.toastr.success('Contact Added!');
      this.currentContact = new Contact();
    });
  }

  private getAllContacts(): void {
    this.contacts = [];
    this.contactService.getAllContacts().subscribe(result => {
      this.contacts = result;
    },
      err => this.toastr.error('FAILED fetching Contacts'));
  }

}
