import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmitted = false;
  user: User;

  constructor(private authenticationService: AuthenticationService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get formControls() { return this.loginForm.controls; }

  public login(): void {
    this.isSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.user = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };

    this.authenticationService.loginUser(this.user)
      .subscribe(
        result => {
          console.log('result', result);
          if (result.id) {
            console.log('result:', result);
            this.authenticationService.setToken(result.email);
            this.router.navigateByUrl('/contacts');
          }
        },
        r => {
          alert(r.error.error);
        });
  }

}
