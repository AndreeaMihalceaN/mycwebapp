import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from 'src/app/shared/models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  baseUrl = 'https://5ee1e01f30deff0016c403bb.mockapi.io/api/v1/contact/';

  constructor(private http: HttpClient) { }

  public getContactById(id: number): Observable<Contact> {
    return this.http.get<Contact>(this.baseUrl + id);
  }

  public getAllContacts(): Observable<Contact[]> {
    return this.http.get<Contact[]>(this.baseUrl);
  }

  public createContact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(this.baseUrl, contact);
  }

  public updateContact(contact: Contact): Observable<Contact> {
    return this.http.put<Contact>(this.baseUrl + contact.id, contact);
  }

  public deleteContact(id: number): Observable<Contact> {
    return this.http.delete<Contact>(this.baseUrl + id);
  }
}
