import { Injectable } from '@angular/core';
import { User } from 'src/app/shared/models/user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  baseUrl = 'https://5ee1e01f30deff0016c403bb.mockapi.io/api/v1/login';

  constructor(private http: HttpClient) { }

  public loginUser(user: User): any {
    return this.http.post<any>(this.baseUrl, user);
  }

  // I will use the email in the absence of the token
  public setToken(userInfo: User): void {
    localStorage.setItem('ACCESS_TOKEN', userInfo.email);
  }

  public isLoggedIn(): boolean {
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  public logout(): void {
    localStorage.removeItem('ACCESS_TOKEN');
  }
}
