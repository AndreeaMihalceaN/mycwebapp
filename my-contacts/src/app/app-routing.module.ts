import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from './modules/contacts/pages/contacts/contacts.component';
import { AuthGuard } from './core/guards/auth.guard';


const routes: Routes = [
  { path: 'contacts', component: ContactsComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
